

public class BirthdayCakeCandles {

    // Complete the birthdayCakeCandles function below.
    static int birthdayCakeCandles(int[] ar) {
        int max = ar[0];
        int countOfMax = 0;

        for (int heightCandle : ar) {
            if (heightCandle > max) {
                max = heightCandle;
            }
        }
        for (int heightCandle : ar) {
            if (max == heightCandle) {
                countOfMax++;
            }
        }
        return countOfMax;
    }

    public static void main(String[] args) {
        int[] ar = new int[] { 5, 5, 3, 4, 5, 7 };
        int result = birthdayCakeCandles(ar);
        System.out.println(result);
    }
}