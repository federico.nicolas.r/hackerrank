
public class PlusMinus {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        int size = arr.length;
        int elementPositive = 0;
        int elementNegative = 0;
        int elementZero = 0;
        for (int value : arr) {
            if (value < 0) {
                elementNegative++;
            } else if (value > 0) {
                elementPositive++;
            } else {
                elementZero++;
            }
        }
        float positiveFraction = (float) elementPositive / size;
        float negativeFraction = (float) elementNegative / size;
        float zeroFraction = (float) elementZero / size;
        System.out.println(positiveFraction);
        System.out.println(negativeFraction);
        System.out.println(zeroFraction);
    }

    public static void main(String[] args) {

        int[] arr = new int[] { 4, 3, -9, 0, 4, 1 };

        plusMinus(arr);
    }
}
