
public class Staircase {

    // Complete the staircase function below.
    static void staircase(int n) {
        char[] vector = new char[n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i + j >= n - 1) {
                    vector[j] = '#';
                } else {
                    vector[j] = 'X';
                }
            }
            System.out.println(vector);
        }
    }

    public static void main(String[] args) {
        staircase(9);
    }
}
