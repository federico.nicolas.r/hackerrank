import java.io.*;

public class DiagonalDifference {

    static int diagonalDifference(int[][] arr) {
        int size = arr[0].length;
        int countOfDiagonalLeftToRight = 0;
        int countOfDiagonalRightToLeft = 0;

        for (int row = 0; row < size; row++) {
            for (int column = 0; column < size; column++) {

                if (row == column) {
                    countOfDiagonalLeftToRight = countOfDiagonalLeftToRight + arr[row][column];
                }

                if (column + row == size - 1) {
                    countOfDiagonalRightToLeft = countOfDiagonalRightToLeft + arr[row][column];
                }
            }
        }
        int result = countOfDiagonalLeftToRight - countOfDiagonalRightToLeft;
        if (result < 0) {
            result = result * -1;
        }
        return result;
    }

    public static void main(String[] args) throws IOException {
        int[][] arr = new int[][] { new int[] { 11, 2, 4 }, new int[] { 4, 5, 6 }, new int[] { 10, 8, -12 }, };

        int result = diagonalDifference(arr);
        System.out.println("RESULTADO: " + result);

    }
}
