
public class MinMaxSum {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(int[] arr) {
        int indexAtMaxValue = 0;
        int indexAtMinValue = 0;
        int max = arr[0];
        int min = arr[0];
        int size = arr.length;

        for (int i = 0; i < size; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexAtMaxValue = i;
            }
            if (arr[i] < min) {
                min = arr[i];
                indexAtMinValue = i;
            }
        }

        long maxSum = 0L;
        long minSum = 0L;
        for (int j = 0; j < size; j++) {
            if (j != indexAtMaxValue) {
                minSum += arr[j];
            }
            if (j != indexAtMinValue) {
                maxSum += arr[j];
            }
        }
        System.out.println(minSum + " " + maxSum);
    }

    public static void main(String[] args) {
        int[] arr = new int[] { 1, 2, 3, 4, 5 };
        miniMaxSum(arr);
    }
}
