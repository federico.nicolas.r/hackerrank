public class TimeConversion {

    static String solution(String s) {
        String PmOrAM = s.substring(s.length() - 2);
        String hour = s.substring(0, 2);
        String minute = s.substring(3, 5);
        String second = s.substring(6, 8);
        Integer hourInteger = Integer.valueOf(hour);
        if (PmOrAM.equals("PM") && !hour.equals("12")) {
            hourInteger += 12;
        }

        hour = hourInteger.toString();
        if (hour.equals("24") || (PmOrAM.equals("AM") && hour.equals("12"))) {
            hour = "00";
        }
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        System.out.println(hour + ":" + minute + ":" + second);
        return null;
    }

    public static void main(String[] args) {

        String result = solution("08:05:45AM");
        System.out.println("RESULT: " + result);
    }
}