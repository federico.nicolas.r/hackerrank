public class ArraysDs {

    static int[] reverseArray(int[] a) {
        int size = a.length;
        int[] reverseArray = new int[size];
        int indexOfReverseArray = 0;
        int i = size - 1;
        System.out.println(size);
        for (i = size - 1; i >= 0; i--) {
            reverseArray[indexOfReverseArray] = a[i];
            indexOfReverseArray++;
        }
        return reverseArray;
    }

    public static void main(String[] args) {
        int[] ar = new int[] { 5, 5, 3, 4, 5, 7 };
        int[] res = reverseArray(ar);

        String result = "";
        for (int i = 0; i < res.length; i++) {
            result = result + res[i] + " ";
        }
        System.out.println(result);
    }
}